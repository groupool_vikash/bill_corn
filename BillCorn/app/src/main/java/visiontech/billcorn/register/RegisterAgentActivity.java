package visiontech.billcorn.register;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import visiontech.billcorn.R;
import visiontech.billcorn.netwrok.AppWebService;
import visiontech.billcorn.otp.VerifyOTPActivity;
import visiontech.billcorn.util.BU;

public class RegisterAgentActivity extends AppCompatActivity {
    private Button btnRegisterAgent;
    private EditText editTxtOpratorId, editTxtAgentName, editTxtMobileNumber;
    private TextView txtPhoneValid, txtOpratorIdValid;
    private Boolean isPhoneValid = false, isOpratorIdValid = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_agent);
        btnRegisterAgent = (Button) findViewById(R.id.btn_register_agent);
        editTxtOpratorId = (EditText) findViewById(R.id.input_oprator_id);
        editTxtAgentName = (EditText) findViewById(R.id.input_name_of_agent);
        editTxtMobileNumber = (EditText) findViewById(R.id.input_mobile_number);
        txtOpratorIdValid = (TextView) findViewById(R.id.txt_valid_oprator_id);
        txtPhoneValid = (TextView) findViewById(R.id.txt_valid_phone);

        editTxtOpratorId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() == 12) {
                    if (isInternet()) {
                        String opratorId = charSequence.toString();
                        try {
                            new CheckOpratorIDTask().execute(AppWebService.BASE_URL, "checkOpratorId", opratorId);
                        } catch (Exception E) {
                            E.printStackTrace();
                        }
                    }
                } else {
                    Log.e("call", "wrong");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        editTxtMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() == 10) {
                    if (isInternet()) {
                        String phone = charSequence.toString();
                        try {
                            new CheckPhoneTask().execute(AppWebService.BASE_URL, "checkPhone", phone);
                        } catch (Exception E) {
                            E.printStackTrace();
                        }
                    }
                } else {
                    Log.e("call", "wrong");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        btnRegisterAgent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editTxtMobileNumber.getText().toString().length() > 7 && editTxtOpratorId.getText().toString().length() > 7 && editTxtAgentName.getText().toString().length() > 2) {
                    if (isPhoneValid == true && isOpratorIdValid == true) {
                     //AgentAregisterTask startActivity(new Intent(RegisterAgentActivity.this, VerifyOTPActivity.class));
                        if (isInternet()) {
                            String phone = editTxtMobileNumber.getText().toString();
                            String opratorId = editTxtOpratorId.getText().toString();
                            String agentName = editTxtAgentName.getText().toString();
                            try {
                                new AgentAregisterTask().execute(AppWebService.BASE_URL, "registerAgent", opratorId, agentName, phone);
                            } catch (Exception E) {
                                E.printStackTrace();
                            }
                        }

                    }
                } else {
                    Toast.makeText(RegisterAgentActivity.this, "Please check Oprator Id, Agent name and phone number", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    private boolean isInternet() {
        if (BU.isConnectingToInternet(RegisterAgentActivity.this)) {
            return true;
        } else {
            Toast.makeText(RegisterAgentActivity.this, "No internet connection.", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    class CheckOpratorIDTask extends AsyncTask<String, Void, String> {
        // public MaterialDialog dialog;

        @Override
        protected void onPreExecute() {
          /*  dialog = new MaterialDialog.Builder(RegisterAgentActivity.this)
                    .title("Checking credential")
                    .content("Please wait..")
                    .progress(true, 0)
                    .cancelable(false)
                    .show();*/
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Map<String, String> paramList = new HashMap<String, String>();
            paramList.put("method", params[1]);
            paramList.put("operator_id", params[2]);
            return AppWebService.sendByPost(params[0], paramList);
        }

        @Override
        protected void onPostExecute(final String result) {
            //  if (dialog.isShowing()) {
            //    dialog.dismiss();
            try {
                if (result != null) {

                    parseUrlData(result);
                }
            } catch (Exception e) {
                Log.e("Download data", "error");

            }
            //}
            super.onPostExecute(result);
        }

        private void parseUrlData(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String returnResult = jsonObject.getString("status");
                if (returnResult.equals("fail")) {
                    isOpratorIdValid = false;
                    txtOpratorIdValid.setTextColor(getResources().getColor(R.color.red));
                    txtOpratorIdValid.setText("Invalid Oprator Id!");
                } else if (returnResult.equals("success")) {
                    isOpratorIdValid = true;
                    txtOpratorIdValid.setTextColor(getResources().getColor(R.color.green));
                    txtOpratorIdValid.setText("valid oprator Id!");
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

    }

    class CheckPhoneTask extends AsyncTask<String, Void, String> {
        // public MaterialDialog dialog;

        @Override
        protected void onPreExecute() {
          /*  dialog = new MaterialDialog.Builder(RegisterAgentActivity.this)
                    .title("Checking credential")
                    .content("Please wait..")
                    .progress(true, 0)
                    .cancelable(false)
                    .show();*/
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Map<String, String> paramList = new HashMap<String, String>();
            paramList.put("method", params[1]);
            paramList.put("phone", params[2]);
            return AppWebService.sendByPost(params[0], paramList);
        }

        @Override
        protected void onPostExecute(final String result) {
            //  if (dialog.isShowing()) {
            //    dialog.dismiss();
            try {
                if (result != null) {

                    parseUrlData(result);
                }
            } catch (Exception e) {
                Log.e("Download data", "error");

            }
            //}
            super.onPostExecute(result);
        }

        private void parseUrlData(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String returnResult = jsonObject.getString("status");
                if (returnResult.equals("fail")) {
                    isPhoneValid = false;
                    txtPhoneValid.setTextColor(getResources().getColor(R.color.red));
                    txtPhoneValid.setText(jsonObject.getString("message"));
                } else if (returnResult.equals("success")) {
                    isPhoneValid = true;
                    txtPhoneValid.setTextColor(getResources().getColor(R.color.green));
                    txtPhoneValid.setText(jsonObject.getString("message"));
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

    }


    class AgentAregisterTask extends AsyncTask<String, Void, String> {
        public MaterialDialog dialog;

        @Override
        protected void onPreExecute() {
           dialog = new MaterialDialog.Builder(RegisterAgentActivity.this)
                    .title("Registrating agent..")
                    .content("Please wait..")
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Map<String, String> paramList = new HashMap<String, String>();
            paramList.put("method", params[1]);
            paramList.put("operator_id", params[2]);
            paramList.put("agent_name", params[3]);
            paramList.put("agent_phone", params[4]);
            return AppWebService.sendByPost(params[0], paramList);
        }

        @Override
        protected void onPostExecute(final String result) {
             if (dialog.isShowing()) {
               dialog.dismiss();
            try {
                if (result != null) {

                    parseUrlData(result);
                }
            } catch (Exception e) {
                Log.e("Download data", "error");

            }
           }
            super.onPostExecute(result);
        }

        private void parseUrlData(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String returnResult = jsonObject.getString("status");
                if (returnResult.equals("fail")) {

                } else if (returnResult.equals("success")) {
                    JSONObject agentData = jsonObject.getJSONObject("data");
                    JSONObject opratorData = jsonObject.getJSONObject("oprator");
                    JSONObject otpMessageDetails = jsonObject.getJSONObject("oprator");
                    String otp = jsonObject.getString("otp");
                    String agentPhone = agentData.getString("agentPhone");
                    String agentName = agentData.getString("agentName");
                    String opratorPhone = opratorData.getString("phone");
                    String opratorId = opratorData.getString("operator_id");
                    String opratorName = opratorData.getString("name");
                    String opratorBusinessName = opratorData.getString("business_name");
                    String opratorAddess = opratorData.getString("address");
                    String opratorCity = opratorData.getString("city");

                    SharedPreferences sharedPref = getSharedPreferences("bill_corn_per", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("operator_id", opratorId);
                    editor.putString("oprator_phone", opratorPhone);
                    editor.putString("oprator_name", opratorName);
                    editor.putString("oprator_business_name", opratorBusinessName);
                    editor.putString("oprator_address", opratorAddess);
                    editor.putString("oprator_city", opratorCity);
                    editor.commit();


                    Intent intent = new Intent(RegisterAgentActivity.this, VerifyOTPActivity.class);
                    intent.putExtra("agentName", agentName);
                    intent.putExtra("agentPhone",agentPhone);
                    intent.putExtra("opratorPhone",opratorPhone);
                    intent.putExtra("otp",otp);
                    finish();
                    startActivity(intent);

                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

    }

}
