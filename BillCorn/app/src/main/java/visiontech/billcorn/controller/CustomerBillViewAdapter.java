package visiontech.billcorn.controller;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


import visiontech.billcorn.R;
import visiontech.billcorn.model.Transaction;
import visiontech.billcorn.util.BU;

/**
 * Created by Ranjan on 3/6/2016.
 */
public class CustomerBillViewAdapter extends ArrayAdapter<Transaction> {

    public ArrayList<Transaction> list;
    Activity activity;
    Transaction objet;
    private int row;
    private LayoutInflater inflater;

    public CustomerBillViewAdapter(Context context, ArrayList<Transaction> list) {
        super(context, R.layout.row_cutomer_paid_bill, list);
        row = R.layout.row_cutomer_paid_bill;
        inflater = LayoutInflater.from(context);
        this.activity = (Activity) context;
        this.list = list;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        try {
            final ViewHolder hold = new ViewHolder();
            convertView = inflater.inflate(row, parent, false);
            hold.linearLayout = (LinearLayout) convertView.findViewById(R.id.linearLayout_row);
            hold.txtMobile = (TextView) convertView.findViewById(R.id.txt_row_customer_phone);
            hold.txtPaidAmount = (TextView) convertView.findViewById(R.id.txt_row_paid_amount);
            hold.txtPaidDate = (TextView) convertView.findViewById(R.id.txt_row_paid_date);
            hold.txtMobile.setText(list.get(position).getCustomerMobile());
            hold.txtPaidAmount.setText("₹ " + list.get(position).getPaidAmount());
            String paidDay = list.get(position).getPaidDay().substring(0, 10);

            SimpleDateFormat month_date = new SimpleDateFormat("MMM dd yyyy", Locale.ENGLISH);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse(paidDay);
            String month_name = month_date.format(date);
            hold.txtPaidDate.setText(month_name);

        } catch (Exception e) {
            Toast.makeText(activity, "Error in layout", Toast.LENGTH_LONG).show();
        }
        return convertView;
    }

    private boolean isInternet() {
        if (BU.isConnectingToInternet(activity)) {
            return true;
        } else {
            Toast.makeText(activity, "No internet connection.", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    class ViewHolder {
        LinearLayout linearLayout;
        public TextView txtMobile, txtPaidAmount, txtPaidDate;

        public ViewHolder() {

        }

        public ViewHolder(LinearLayout linearLayout, TextView txtMobile, TextView txtPaidAmount, TextView txtPaidDate) {
            this.linearLayout = linearLayout;
            this.txtMobile = txtMobile;
            this.txtPaidAmount = txtPaidAmount;
            this.txtPaidDate = txtPaidDate;
        }
    }


}
