package visiontech.billcorn.otp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mukesh.OtpView;

import visiontech.billcorn.R;
import visiontech.billcorn.home.AgentLandingActivity;

public class VerifyOTPActivity extends AppCompatActivity {
    private String otp, agentName, agentPhone, opratorPhone;
    private OtpView otpView;
    private Button btnVerify;
    private TextView txtOtpMessage, txtResendOtp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);
        otpView = (OtpView) findViewById(R.id.otp_view);
        btnVerify = (Button)findViewById(R.id.btn_verify);
        txtOtpMessage = (TextView) findViewById(R.id.txt_opt_msg);
        txtResendOtp = (TextView) findViewById(R.id.txt_resend);
        Intent extra = getIntent();
        if(extra!=null){
            agentName = extra.getStringExtra("agentName");
            agentPhone = extra.getStringExtra("agentPhone");
            opratorPhone = extra.getStringExtra("opratorPhone");
            otp = extra.getStringExtra("otp");
            if(opratorPhone != null ){
                  String threeDigit = opratorPhone.substring(opratorPhone.length() - 3);
                  String msg = "OTP has been sent to Oprator phone XXXXXXX"+threeDigit;
                  txtOtpMessage.setText(msg);
                }else{
                String threeDigit = agentPhone.substring(agentPhone.length() - 3);
                String msg = "OTP has been sent to agent phone XXXXXXX"+threeDigit;
                txtOtpMessage.setText(msg);
            }
        }
        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String localOtp = otpView.getOTP();
                if(otp.equals(localOtp)){
                    SharedPreferences sharedPref = getSharedPreferences("bill_corn_per", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("agentName", agentName);
                    editor.putString("agentPhone", agentPhone);
                    editor.putBoolean("isOtpVrified", true);
                    editor.commit();
                    finish();
                    startActivity(new Intent(VerifyOTPActivity.this, AgentLandingActivity.class));
                }

            }
        });

    }
}
