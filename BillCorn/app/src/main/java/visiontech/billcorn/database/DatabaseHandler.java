package visiontech.billcorn.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import visiontech.billcorn.model.Transaction;

/**
 * Created by Ranjan on 3/9/2017.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 2;

    // Database Name
    private static final String DATABASE_NAME = "billCorn";


    private static final String TABLE_TRANSACTIONS = "table_transactions";

    private static final String TRANSACTIONS_ID = "id";
    private static final String TRANSACTIONS_CUST_PHONE = "cust_phone";
    private static final String TRANSACTIONS_AGENT_PHONE = "agent_phone";
    private static final String TRANSACTIONS_YEAR = "paid_year";
    private static final String TRANSACTIONS_MONTH = "paid_month";
    private static final String TRANSACTIONS_PAID_AMOUNT = "paid_amoun";
    private static final String TRANSACTIONS_GST_AMOUNT = "gst_amount";
    private static final String TRANSACTIONS_DATE = "date";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TRANSACTIONS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_TRANSACTIONS + "("
                + TRANSACTIONS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TRANSACTIONS_CUST_PHONE + " VARCHAR,"
                + TRANSACTIONS_AGENT_PHONE + " VARCHAR," + TRANSACTIONS_YEAR + " INTEGER,"
                + TRANSACTIONS_MONTH + " INTEGER," + TRANSACTIONS_PAID_AMOUNT + " INTEGER,"
                + TRANSACTIONS_GST_AMOUNT + " INTEGER," + TRANSACTIONS_DATE + " VARCHAR" + ")";

        db.execSQL(CREATE_TRANSACTIONS_TABLE);
    }

    // Upgrading database
    public void insertTransactions(Transaction list) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TRANSACTIONS_ID, list.getId());
        values.put(TRANSACTIONS_CUST_PHONE, list.getCustomerMobile());
        values.put(TRANSACTIONS_AGENT_PHONE, list.getAgentPhone());
        values.put(TRANSACTIONS_YEAR, list.getPaidYear());
        values.put(TRANSACTIONS_MONTH, list.getPaidMonth());
        values.put(TRANSACTIONS_PAID_AMOUNT, list.getPaidAmount());
        values.put(TRANSACTIONS_GST_AMOUNT, list.getGstAmount());
        values.put(TRANSACTIONS_DATE, list.getPaidDay());
        // Inserting Row
        db.insert(TABLE_TRANSACTIONS, null, values);
        Log.e("Data", "Inserted");
        db.close(); // Closing database connection
    }

    public ArrayList<Transaction> getLocalTransactions() {
        ArrayList<Transaction> results = new ArrayList<Transaction>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("SELECT * FROM " + TABLE_TRANSACTIONS, null);
            if (cursor != null && cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    Transaction object = new Transaction();
                    object.setId(cursor.getInt(cursor.getColumnIndex(TRANSACTIONS_ID)));
                    object.setAgentPhone(cursor.getString(cursor.getColumnIndex(TRANSACTIONS_AGENT_PHONE)));
                    object.setCustomerMobile(cursor.getString(cursor.getColumnIndex(TRANSACTIONS_CUST_PHONE)));
                    object.setPaidAmount(cursor.getInt(cursor.getColumnIndex(TRANSACTIONS_PAID_AMOUNT)));
                    object.setGstAmount(cursor.getInt(cursor.getColumnIndex(TRANSACTIONS_GST_AMOUNT)));
                    object.setPaidDay(cursor.getString(cursor.getColumnIndex(TRANSACTIONS_DATE)));
                    object.setPaidMonth(cursor.getInt(cursor.getColumnIndex(TRANSACTIONS_MONTH)));
                    object.setPaidYear(cursor.getInt(cursor.getColumnIndex(TRANSACTIONS_YEAR)));
                    results.add(object);
                    Log.e("TAG", "selected");
                }
            }

        } catch (Exception e) {
            Log.e("TAG", "Exception " + e.toString());
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return results;

    }

    public ArrayList<Transaction> getLocalTransactionsByDate(String selectedDate) {
        ArrayList<Transaction> results = new ArrayList<Transaction>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("SELECT * FROM " + TABLE_TRANSACTIONS + " WHERE " + TRANSACTIONS_DATE + " LIKE '%" + selectedDate + "%'", null);
            if (cursor != null && cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    Transaction object = new Transaction();
                    object.setId(cursor.getInt(cursor.getColumnIndex(TRANSACTIONS_ID)));
                    object.setAgentPhone(cursor.getString(cursor.getColumnIndex(TRANSACTIONS_AGENT_PHONE)));
                    object.setCustomerMobile(cursor.getString(cursor.getColumnIndex(TRANSACTIONS_CUST_PHONE)));
                    object.setPaidAmount(cursor.getInt(cursor.getColumnIndex(TRANSACTIONS_PAID_AMOUNT)));
                    object.setGstAmount(cursor.getInt(cursor.getColumnIndex(TRANSACTIONS_GST_AMOUNT)));
                    object.setPaidDay(cursor.getString(cursor.getColumnIndex(TRANSACTIONS_DATE)));
                    object.setPaidMonth(cursor.getInt(cursor.getColumnIndex(TRANSACTIONS_MONTH)));
                    object.setPaidYear(cursor.getInt(cursor.getColumnIndex(TRANSACTIONS_YEAR)));
                    results.add(object);
                    Log.e("TAG", "selected");
                }
            }

        } catch (Exception e) {
            Log.e("TAG", "Exception " + e.toString());
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return results;
    }

    public float getLocalTransactionsSumByDate(String selectedDate) {

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = null;
        try {
            if (selectedDate != null) {
                cursor = db.rawQuery("SELECT SUM(" + TRANSACTIONS_PAID_AMOUNT + ") FROM " + TABLE_TRANSACTIONS + " WHERE " + TRANSACTIONS_DATE + " LIKE '%" + selectedDate + "%'", null);
                if (cursor != null && cursor.getCount() > 0) {
                    if (cursor.moveToFirst()) {
                        return cursor.getFloat(0);
                    }

                }
            } else {
                cursor = db.rawQuery("SELECT SUM(" + TRANSACTIONS_PAID_AMOUNT + ") FROM " + TABLE_TRANSACTIONS, null);
                if (cursor != null && cursor.getCount() > 0) {
                    if (cursor.moveToFirst()) {
                        return cursor.getFloat(0);
                    }

                }
            }

        } catch (Exception e) {
            Log.e("TAG", "Exception " + e.toString());
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return 0;
    }

    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE  FROM " + TABLE_TRANSACTIONS);
        db.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRANSACTIONS);
        // Create tables again
        onCreate(db);
    }
}