package visiontech.billcorn.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import android.util.Log;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class BU {
	public static boolean isConnectingToInternet(Context appContext) {
		// Method to check internet connection
		ConnectivityManager connectivity = (ConnectivityManager) appContext
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
		}
		return false;
	}

	public static String getIMEI(Context con) {
		String identifier = null;
		TelephonyManager tm;
		try {
			tm = (TelephonyManager) con
					.getSystemService(Context.TELEPHONY_SERVICE);
			if (tm != null)
				identifier = tm.getDeviceId();
			if (identifier == null || identifier.length() == 0)
				identifier = Secure.getString(con.getContentResolver(),
						Secure.ANDROID_ID);
		} catch (Exception e) {
			return "Not Available";
		}
		return identifier;
	}

	public String getLocalIpAddress() {

		// TODO get IP Address
		try {
			for (Enumeration<NetworkInterface> en = NetworkInterface
					.getNetworkInterfaces(); en.hasMoreElements();) {
				NetworkInterface intf = en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr = intf
						.getInetAddresses(); enumIpAddr.hasMoreElements();) {
					InetAddress inetAddress = enumIpAddr.nextElement();
					if (!inetAddress.isLoopbackAddress()) {
						String ip = Formatter.formatIpAddress(inetAddress
								.hashCode());
						// Log.i(TAG, "***** IP="+ ip);
					//	LTU.LI("BU getLocalIpAddress", "IP Address :" + ip);
						return ip;
					}
				}
			}
		} catch (SocketException ex) {
			Log.e("BU getLocalIpAddress", ex.toString());
		}
		return null;
	}


}
