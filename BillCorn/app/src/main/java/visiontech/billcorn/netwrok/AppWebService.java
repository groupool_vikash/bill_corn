package visiontech.billcorn.netwrok;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Ranjan on 28/8/2017.
 */
public class AppWebService {

    public static String BASE_URL = "http://billcorn.com/bill_corn_api/bill_corn.php";
   // public static String BASE_URL = "http://visiontech9.com/bill/bill_corn.php";

    public static String sendByPost(String url,Map<String, String> params) {
        URL obj;
        HttpURLConnection con ;
        int responseCode = 0;
        StringBuffer response = new StringBuffer();
        try {
            obj = new URL(url);
            con=(HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
//		con.setRequestProperty("User-Agent", USER_AGENT);
            StringBuffer requestParams = new StringBuffer();
            // For POST only - START
            //con.setDoOutput(true);
            if (params != null && params.size() > 0) {

                con.setDoOutput(true); // true indicates POST request

                // creates the params string, encode them using URLEncoder
                Iterator<String> paramIterator = params.keySet().iterator();
                while (paramIterator.hasNext()) {
                    String key = paramIterator.next();
                    String value = params.get(key);
                    requestParams.append(URLEncoder.encode(key, "UTF-8"));
                    requestParams.append("=").append(
                            URLEncoder.encode(value, "UTF-8"));
                    requestParams.append("&");
                }

                // sends POST data
                OutputStreamWriter writer = new OutputStreamWriter(
                        con.getOutputStream());
                writer.write(requestParams.toString());
                writer.flush();

            }
            responseCode = con.getResponseCode();
            System.out.println("POST Response Code :: " + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) { //success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));
                String inputLine;

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        if(responseCode == HttpURLConnection.HTTP_OK){
            return response.toString();
        }else {
            return "0";
        }

    }

    public static String sendByGet(String url)
    {
        String result = null;
        URL obj = null;
        InputStream inputStream;
        int responseCode=0 ;
        try {
            obj = new URL(url);

            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("REQUEST_TOKEN","754ad97d20b99511b8b58d0e70a1d1b7");
            con.setConnectTimeout(30000);
            responseCode = con.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream stream = con.getInputStream();
                InputStreamReader isReader = new InputStreamReader(stream );
                BufferedReader br = new BufferedReader(isReader );
                StringBuilder stringBuilder = new StringBuilder();
                String line = "0";
                while ((line = br.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                }
                stream.close();
                result = stringBuilder.toString();

            }else {
                System.out.println("POST request not worked");
                return "0";
            }
        }catch(Exception e){
            e.printStackTrace();
            return "0";
        }
        return  result;
    }
}
