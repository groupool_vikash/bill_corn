package visiontech.billcorn;
// oJKdcSs(iBd~
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.daimajia.androidanimations.library.Techniques;
import com.viksaa.sssplash.lib.activity.AwesomeSplash;
import com.viksaa.sssplash.lib.cnst.Flags;
import com.viksaa.sssplash.lib.model.ConfigSplash;

import visiontech.billcorn.home.AgentLandingActivity;
import visiontech.billcorn.loginorregister.AgentLoginOrRegisterActivity;
import visiontech.billcorn.permission.PermissionsActivity;
import visiontech.billcorn.permission.PermissionsChecker;

public class MainActivity extends AwesomeSplash {
    private static final String[] PERMISSIONS = new String[]{Manifest.permission.INTERNET,
            Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.ACCESS_NETWORK_STATE};
    PermissionsChecker checker;
    @Override
    public void initSplash(ConfigSplash configSplash) {

			/* you don't have to override every property */
        checker = new PermissionsChecker(getApplicationContext());
        if (checker.lacksPermissions(PERMISSIONS)) {
            startPermissionsActivity(PERMISSIONS);

        }
        //Customize Circular Reveal
        configSplash.setBackgroundColor(R.color.white); //any color you want form colors.xml
        configSplash.setAnimCircularRevealDuration(2000); //int ms
        configSplash.setRevealFlagX(Flags.REVEAL_LEFT);  //or Flags.REVEAL_LEFT
        configSplash.setRevealFlagY(Flags.REVEAL_TOP); //or Flags.REVEAL_TOP

        //Customize Logo
        configSplash.setLogoSplash(R.drawable.logo2); //or any other drawable
        configSplash.setAnimLogoSplashDuration(2000); //int ms
        configSplash.setAnimLogoSplashTechnique(Techniques.Bounce); //choose one form Techniques (ref: https://github.com/daimajia/AndroidViewAnimations)

        //Customize Title
        configSplash.setTitleSplash("");
        configSplash.setTitleTextColor(R.color.white);
        configSplash.setTitleTextSize(30f); //float value
        configSplash.setAnimTitleDuration(3000);
        configSplash.setAnimTitleTechnique(Techniques.FlipInX);

    }
    private void startPermissionsActivity(String[] permission) {
        PermissionsActivity.startActivityForResult(this, 0, permission);
    }
    @Override
    public void animationsFinished() {
        checker = new PermissionsChecker(MainActivity.this);
        if (checker.lacksPermissions(PERMISSIONS)) {
            startPermissionsActivity(PERMISSIONS);

        }
        SharedPreferences localData = getSharedPreferences("bill_corn_per", Context.MODE_PRIVATE);

        Boolean isOtpVerified = localData.getBoolean("isOtpVrified", false);
        if (isOtpVerified) {
            finish();
            startActivity(new Intent(MainActivity.this, AgentLandingActivity.class));
        } else {
            finish();
            startActivity(new Intent(MainActivity.this, AgentLoginOrRegisterActivity.class));
        }
    }
}
