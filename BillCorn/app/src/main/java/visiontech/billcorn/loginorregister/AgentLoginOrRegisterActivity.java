package visiontech.billcorn.loginorregister;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import visiontech.billcorn.R;
import visiontech.billcorn.home.AgentLandingActivity;
import visiontech.billcorn.netwrok.AppWebService;
import visiontech.billcorn.otp.VerifyOTPActivity;
import visiontech.billcorn.register.RegisterAgentActivity;
import visiontech.billcorn.util.BU;

public class AgentLoginOrRegisterActivity extends AppCompatActivity {

    private TextView txtRegisterAsAgent;
    private Button btnAgentLogin;
    private EditText editTextPhone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_login_or_register);
        txtRegisterAsAgent = (TextView)findViewById(R.id.txt_register_agent);
        btnAgentLogin = (Button)findViewById(R.id.btn_login_agent);
        editTextPhone = (EditText)findViewById(R.id.edit_txt_phone_login);
        btnAgentLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 if(editTextPhone.getText().toString().length()==10){
                     if (isInternet()) {
                         String phone = editTextPhone.getText().toString();
                         try {
                             new AgentLoginTask().execute(AppWebService.BASE_URL, "agentLogin", phone);
                         } catch (Exception E) {
                             E.printStackTrace();
                         }
                     }
                 }else{
                     Toast.makeText(AgentLoginOrRegisterActivity.this,"Please check the mobile number!!",Toast.LENGTH_SHORT).show();
                 }
            }
        });
        txtRegisterAsAgent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                startActivity(new Intent(AgentLoginOrRegisterActivity.this, RegisterAgentActivity.class));
            }
        });
    }

    class AgentLoginTask extends AsyncTask<String, Void, String> {
        public MaterialDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MaterialDialog.Builder(AgentLoginOrRegisterActivity.this)
                    .title("Logining agent..")
                    .content("Please wait..")
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Map<String, String> paramList = new HashMap<String, String>();
            paramList.put("method", params[1]);
            paramList.put("agent_phone", params[2]);
            return AppWebService.sendByPost(params[0], paramList);
        }

        @Override
        protected void onPostExecute(final String result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
                try {
                    if (result != null) {

                        parseUrlData(result);
                    }
                } catch (Exception e) {
                    Log.e("Download data", "error");

                }
            }
            super.onPostExecute(result);
        }

        private void parseUrlData(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String returnResult = jsonObject.getString("status");
                if (returnResult.equals("fail")) {
                   Toast.makeText(AgentLoginOrRegisterActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                } else if (returnResult.equals("success")) {
                    JSONObject agentData = jsonObject.getJSONObject("data");
                    JSONObject opratorData = jsonObject.getJSONObject("oprator_data");
                    JSONObject otpStatusData = jsonObject.getJSONObject("otp_status");
                    String otp = jsonObject.getString("otp");
                    String agentPhone = agentData.getString("phone");
                    String agentName = agentData.getString("name");
                    String opratorId = opratorData.getString("operator_id");
                    String opratorName = opratorData.getString("name");
                    String opratorBusinessName = opratorData.getString("business_name");
                    String opratorPhone = opratorData.getString("phone");
                    String opratorAddess = opratorData.getString("address");
                    String opratorCity = opratorData.getString("city");

                    SharedPreferences sharedPref = getSharedPreferences("bill_corn_per", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("operator_id", opratorId);
                    editor.putString("oprator_phone", opratorPhone);
                    editor.putString("oprator_name", opratorName);
                    editor.putString("oprator_business_name", opratorBusinessName);
                    editor.putString("oprator_address", opratorAddess);
                    editor.putString("oprator_city", opratorCity);
                    editor.commit();

                    Intent intent = new Intent(AgentLoginOrRegisterActivity.this, VerifyOTPActivity.class);
                    intent.putExtra("agentName", agentName);
                    intent.putExtra("agentPhone",agentPhone);
                    intent.putExtra("otp",otp);
                    finish();
                    startActivity(intent);

                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

    }
    private boolean isInternet() {
        if (BU.isConnectingToInternet(AgentLoginOrRegisterActivity.this)) {
            return true;
        } else {
            Toast.makeText(AgentLoginOrRegisterActivity.this, "No internet connection.", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

}
