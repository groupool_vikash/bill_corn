package visiontech.billcorn.home;

import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gelitenight.waveview.library.WaveView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import visiontech.billcorn.R;
import visiontech.billcorn.animation.CanvasView;
import visiontech.billcorn.animation.WaveHelper;
import visiontech.billcorn.controller.CustomerBillViewAdapter;
import visiontech.billcorn.model.Transaction;
import visiontech.billcorn.netwrok.AppWebService;
import visiontech.billcorn.register.RegisterAgentActivity;
import visiontech.billcorn.util.BU;

import static android.view.View.GONE;

public class CustomerHistoryActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private SearchView searchView;
    private ListView listView;
    private CustomerBillViewAdapter customerBillViewAdapter;
    private WaveView waveView;
    private WaveHelper mWaveHelper;
    private CanvasView canvas = null;
    private int mBorderColor = Color.parseColor("#44FFFFFF");
    private int mBorderWidth = 5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_history);
        toolbar = (Toolbar) findViewById(R.id.toolbar_customer_history);
        searchView = (SearchView) findViewById(R.id.search_view_cutomer);
        listView = (ListView)findViewById(R.id.list_view_customer_date);
        waveView = (WaveView) findViewById(R.id.wave);

        // waveView.setBorder(mBorderWidth, mBorderColor);


        mWaveHelper = new WaveHelper(waveView);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");

        waveView.setShapeType(WaveView.ShapeType.CIRCLE);
        waveView.setWaveColor(
                Color.parseColor("#88b8f1ed"),
                Color.parseColor("#b8f1ed"));
        mBorderColor = Color.parseColor("#b8f1ed");
        waveView.setBorder(mBorderWidth, mBorderColor);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
               if(isInternet()) {
                   try {
                       new GetCustomerHistoryTask().execute(AppWebService.BASE_URL, "getCustomerPayHistory", query);
                   } catch (Exception e) {
                       e.printStackTrace();
                   }
                   searchView.clearFocus();
               }
               return true;

            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    public CanvasView getCanvas() {
        return this.canvas;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mWaveHelper.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mWaveHelper.start();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }


    class GetCustomerHistoryTask extends AsyncTask<String, Void, String> {
         public MaterialDialog dialog;
         private  Transaction transaction;
         private ArrayList<Transaction> list;
        @Override
        protected void onPreExecute() {
            dialog = new MaterialDialog.Builder(CustomerHistoryActivity.this)
                    .title("Checking Customer")
                    .content("Please wait..")
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Map<String, String> paramList = new HashMap<String, String>();
            paramList.put("method", params[1]);
            paramList.put("customer_mobile", params[2]);
            return AppWebService.sendByPost(params[0], paramList);
        }

        @Override
        protected void onPostExecute(final String result) {
              if (dialog.isShowing()) {
                dialog.dismiss();
            try {
                if (result != null) {

                    parseUrlData(result);
                }
            } catch (Exception e) {
                Log.e("Download data", "error");

            }
            }
            super.onPostExecute(result);
        }

        private void parseUrlData(String result) {
            try {
                list = new ArrayList<Transaction>();
                JSONObject jsonObject = new JSONObject(result);
                String returnResult = jsonObject.getString("status");
                if (returnResult.equals("fail")) {
                    listView.setAdapter(null);
                    listView.setVisibility(View.GONE);
                    waveView.setVisibility(View.VISIBLE);
                    Toast.makeText(CustomerHistoryActivity.this,"No Customer record found with this number!",Toast.LENGTH_SHORT).show();
                } else if (returnResult.equals("success")) {
                    listView.setVisibility(View.VISIBLE);
                    waveView.setVisibility(GONE);

                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    for(int i=0; i<jsonArray.length();i++){
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        transaction = new Transaction();
                        transaction.setId(jsonObject1.getInt("id"));
                        transaction.setAgentPhone(jsonObject1.getString("agent_phone"));
                        transaction.setCustomerMobile(jsonObject1.getString("cust_mobile"));
                        transaction.setGstAmount(jsonObject1.getInt("gst_amount"));
                        transaction.setPaidAmount(jsonObject1.getInt("paid_amount"));
                        transaction.setPaidDay(jsonObject1.getString("paid_day"));
                        transaction.setPaidMonth(jsonObject1.getInt("paid_month"));
                        transaction.setPaidYear(jsonObject1.getInt("paid_year"));
                        list.add(transaction);
                    }

                    if(list!=null){
                        setListViewAdapter(list);
                    }

                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

    }
    private void setListViewAdapter(ArrayList<Transaction> transactionArrayList) {
        try {
            customerBillViewAdapter = new CustomerBillViewAdapter(CustomerHistoryActivity.this, transactionArrayList);
            this.listView.setAdapter(customerBillViewAdapter);
        } catch (Exception e) {
            Toast.makeText(CustomerHistoryActivity.this, "Exception in adapter", Toast.LENGTH_LONG).show();
        }
    }
    private boolean isInternet() {
        if (BU.isConnectingToInternet(CustomerHistoryActivity.this)) {
            return true;
        } else {
            Toast.makeText(CustomerHistoryActivity.this, "No internet connection.", Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}
