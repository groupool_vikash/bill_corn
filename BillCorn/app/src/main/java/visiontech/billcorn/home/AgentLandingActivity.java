package visiontech.billcorn.home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import visiontech.billcorn.R;
import visiontech.billcorn.database.DatabaseHandler;
import visiontech.billcorn.home.bill.CustomerBillActivity;
import visiontech.billcorn.loginorregister.AgentLoginOrRegisterActivity;
import visiontech.billcorn.netwrok.AppWebService;
import visiontech.billcorn.util.BU;

public class AgentLandingActivity extends AppCompatActivity {
    private Button btnProceedToPay;
    private EditText editTextCustmerMobile;

    //canAppRun
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_landing);
        btnProceedToPay = (Button) findViewById(R.id.btn_proceed_to_pay);
        editTextCustmerMobile = (EditText) findViewById(R.id.input_mobile_number_cust);
        SharedPreferences localData = getSharedPreferences("bill_corn_per", Context.MODE_PRIVATE);

        String agentName = localData.getString("agentName", null);
        String agentPhone = localData.getString("agentPhone", null);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        try {
            new GetApplicationStatusTask().execute("http://visiontech9.com/bill/bill_corn.php", "canAppRun");
        } catch (Exception e) {
            e.printStackTrace();
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Bill Corn");
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.header)
                .addProfiles(
                        new ProfileDrawerItem().withName(agentName).withEmail(agentPhone).withIcon(FontAwesome.Icon.faw_user)
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();

        Drawer result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.drawer_item_profile).withIcon(FontAwesome.Icon.faw_user_circle),
                        new SecondaryDrawerItem().withName(R.string.drawer_item_history).withIcon(FontAwesome.Icon.faw_history),
                        new SecondaryDrawerItem().withName(R.string.drawer_item_cust_history).withIcon(FontAwesome.Icon.faw_history),
                        new SecondaryDrawerItem().withName(R.string.drawer_item_logout).withIcon(FontAwesome.Icon.faw_sign_out)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem instanceof Nameable) {
                            if (position == 1) {

                            } else if (position == 2) {
                                startActivity(new Intent(AgentLandingActivity.this, AgentHistoryActivity.class));
                            } else if (position == 3) {
                                startActivity(new Intent(AgentLandingActivity.this, CustomerHistoryActivity.class));
                            } else if (position == 4) {
                                SharedPreferences sharedPref = getSharedPreferences("bill_corn_per", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.clear();
                                editor.commit();

                                DatabaseHandler db = new DatabaseHandler(AgentLandingActivity.this);
                                db.deleteAll();

                                finish();
                                startActivity(new Intent(AgentLandingActivity.this, AgentLoginOrRegisterActivity.class));
                            }
                        }

                        return false;
                    }
                }).build();


        btnProceedToPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editTextCustmerMobile.getText().toString().length() == 10) {
                    try {
                        if (isInternet()) {
                            new GetCustomerTask().execute(AppWebService.BASE_URL, "getCustomerDetails", editTextCustmerMobile.getText().toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(AgentLandingActivity.this, "Invalide Mobile Number", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    class GetApplicationStatusTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Map<String, String> paramList = new HashMap<String, String>();
            paramList.put("method", params[1]);
            return AppWebService.sendByPost(params[0], paramList);
        }

        @Override
        protected void onPostExecute(final String result) {

            try {
                if (result != null) {
                    parseUrlData(result);
                }
            } catch (Exception e) {
                Log.e("Download data", "error");

            }

            super.onPostExecute(result);
        }

        private void parseUrlData(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String returnResult = jsonObject.getString("status");
                if (returnResult.equals("fail")) {
                    new SweetAlertDialog(AgentLandingActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Application terminated by the developer..")
                            .setContentText(jsonObject.getString("message")).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            finish();
                        }
                    })
                            .show();
                } else if (returnResult.equals("success")) {

                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

    }

    class GetCustomerTask extends AsyncTask<String, Void, String> {
        public MaterialDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MaterialDialog.Builder(AgentLandingActivity.this)
                    .title("Logining agent..")
                    .content("Please wait..")
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Map<String, String> paramList = new HashMap<String, String>();
            paramList.put("method", params[1]);
            paramList.put("customer_phone", params[2]);
            return AppWebService.sendByPost(params[0], paramList);
        }

        @Override
        protected void onPostExecute(final String result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
                try {
                    if (result != null) {

                        parseUrlData(result);
                    }
                } catch (Exception e) {
                    Log.e("Download data", "error");

                }
            }
            super.onPostExecute(result);
        }

        private void parseUrlData(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String returnResult = jsonObject.getString("status");
                if (returnResult.equals("fail")) {
                    Toast.makeText(AgentLandingActivity.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                } else if (returnResult.equals("success")) {
                    JSONObject customerData = jsonObject.getJSONObject("data");
                    String customerName = customerData.getString("cust_name");
                    String customerMobile = customerData.getString("cust_mobile");
                    String customerStvNo = customerData.getString("stv_no");
                    String customerMonthlyBill = customerData.getString("cust_m_bill");
                    String customerYealyBill = customerData.getString("cust_y_bill");
                    String customerDue = customerData.getString("cust_due_amount");
                    String customerAvailable = customerData.getString("cust_avail_amount");
                    Intent intent = new Intent(AgentLandingActivity.this, CustomerBillActivity.class);
                    intent.putExtra("customerName", customerName);
                    intent.putExtra("customerMobile", customerMobile);
                    intent.putExtra("customerStvNo", customerStvNo);
                    intent.putExtra("customerMonthlyBill", customerMonthlyBill);
                    intent.putExtra("customerYealyBill", customerYealyBill);
                    intent.putExtra("customerDue", customerDue);
                    intent.putExtra("customerAvailable", customerAvailable);
                    startActivity(intent);
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

    }

    private boolean isInternet() {
        if (BU.isConnectingToInternet(AgentLandingActivity.this)) {
            return true;
        } else {
            Toast.makeText(AgentLandingActivity.this, "No internet connection.", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

}
