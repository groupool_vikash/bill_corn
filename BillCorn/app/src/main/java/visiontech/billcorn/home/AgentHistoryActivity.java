package visiontech.billcorn.home;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import visiontech.billcorn.R;
import visiontech.billcorn.controller.AgentHistoryBillViewAdapter;
import visiontech.billcorn.controller.CustomerBillViewAdapter;
import visiontech.billcorn.database.DatabaseHandler;
import visiontech.billcorn.home.bill.CustomerBillActivity;
import visiontech.billcorn.model.Transaction;
import visiontech.billcorn.netwrok.AppWebService;
import visiontech.billcorn.util.BU;

public class AgentHistoryActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    Toolbar toolbar;
    ListView listView;
    TextView txtSelectDate, txtTotalAmount;
    DatabaseHandler db;
    ArrayList<Transaction> list;
    private AgentHistoryBillViewAdapter agentHistoryBillViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_history);
        toolbar = (Toolbar) findViewById(R.id.toolbar_agent_history);
        listView = (ListView) findViewById(R.id.listview_agent_history);
        txtSelectDate = (TextView) findViewById(R.id.txt_select_date);
        txtTotalAmount = (TextView) findViewById(R.id.txt_total_amount);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        db = new DatabaseHandler(AgentHistoryActivity.this);
        list = db.getLocalTransactions();
        if (list != null && list.size() != 0) {
            setListViewAdapter(list);
            txtTotalAmount.setText("₹ "+db.getLocalTransactionsSumByDate(null)+"");
        } else {
            if (isInternet()) {
                try {
                    SharedPreferences sharedPref = getSharedPreferences("bill_corn_per", Context.MODE_PRIVATE);
                    String agentPhone = sharedPref.getString("agentPhone", null);
                    if (agentPhone != null) {
                        new GetAgentHistoryTask().execute(AppWebService.BASE_URL, "getAgentPayHistory", agentPhone);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        txtSelectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        AgentHistoryActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.showYearPickerFirst(true);
                dpd.show(getSupportFragmentManager(), "Datepickerdialog");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String selectedDate = null;
        String month = null;
        String day = null;
        if (monthOfYear + 1 < 10) {
            month = "0" + (monthOfYear + 1);
        } else {
            month = "" + (monthOfYear + 1);
        }

        if (dayOfMonth < 10) {
            day = "0" + dayOfMonth;
        } else {
            day = "" + dayOfMonth;
        }

        selectedDate = year + "-" + month + "-" + day;
        txtSelectDate.setText(selectedDate);
        ArrayList<Transaction> transactions = db.getLocalTransactionsByDate(selectedDate);
        if (transactions.size() > 0) {
            setListViewAdapter(transactions);
            float totalAmount = db.getLocalTransactionsSumByDate(selectedDate);
            txtTotalAmount.setText("₹ "+totalAmount+"");
        } else {
            txtTotalAmount.setText("₹ 00.00");
            listView.setAdapter(null);
            Toast.makeText(AgentHistoryActivity.this, "No payment made on " + selectedDate, Toast.LENGTH_SHORT).show();
        }
    }

    class GetAgentHistoryTask extends AsyncTask<String, Void, String> {
        public MaterialDialog dialog;
        private Transaction transaction;
        private ArrayList<Transaction> list;

        @Override
        protected void onPreExecute() {
            dialog = new MaterialDialog.Builder(AgentHistoryActivity.this)
                    .title("Checking Customer")
                    .content("Please wait..")
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Map<String, String> paramList = new HashMap<String, String>();
            paramList.put("method", params[1]);
            paramList.put("agent_phone", params[2]);
            return AppWebService.sendByPost(params[0], paramList);
        }

        @Override
        protected void onPostExecute(final String result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
                try {
                    if (result != null) {

                        parseUrlData(result);
                    }
                } catch (Exception e) {
                    Log.e("Download data", "error");

                }
            }
            super.onPostExecute(result);
        }

        private void parseUrlData(String result) {
            try {
                list = new ArrayList<Transaction>();
                JSONObject jsonObject = new JSONObject(result);
                String returnResult = jsonObject.getString("status");
                if (returnResult.equals("fail")) {
                    listView.setAdapter(null);
                    Toast.makeText(AgentHistoryActivity.this, "No record found with this number!", Toast.LENGTH_SHORT).show();
                } else if (returnResult.equals("success")) {
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        transaction = new Transaction();
                        transaction.setId(jsonObject1.getInt("id"));
                        transaction.setAgentPhone(jsonObject1.getString("agent_phone"));
                        transaction.setCustomerMobile(jsonObject1.getString("cust_mobile"));
                        transaction.setGstAmount(jsonObject1.getInt("gst_amount"));
                        transaction.setPaidAmount(jsonObject1.getInt("paid_amount"));
                        transaction.setPaidDay(jsonObject1.getString("paid_day"));
                        transaction.setPaidMonth(jsonObject1.getInt("paid_month"));
                        transaction.setPaidYear(jsonObject1.getInt("paid_year"));
                        db.insertTransactions(transaction);
                        list.add(transaction);
                    }

                    if (list != null) {
                        setListViewAdapter(list);
                        txtTotalAmount.setText(db.getLocalTransactionsSumByDate(null)+"");
                    }

                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

    }

    private void setListViewAdapter(ArrayList<Transaction> transactionArrayList) {
        try {
            agentHistoryBillViewAdapter = new AgentHistoryBillViewAdapter(AgentHistoryActivity.this, transactionArrayList);
            this.listView.setAdapter(agentHistoryBillViewAdapter);
        } catch (Exception e) {
            Toast.makeText(AgentHistoryActivity.this, "Exception in adapter", Toast.LENGTH_LONG).show();
        }
    }

    private boolean isInternet() {
        if (BU.isConnectingToInternet(AgentHistoryActivity.this)) {
            return true;
        } else {
            Toast.makeText(AgentHistoryActivity.this, "No internet connection.", Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}
