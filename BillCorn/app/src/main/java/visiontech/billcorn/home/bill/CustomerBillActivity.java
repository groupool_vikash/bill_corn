package visiontech.billcorn.home.bill;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.FragmentManager;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CursorAdapter;
import android.widget.TextView;
import android.widget.Toast;


import com.aem.api.AEMPrinter;
import com.aem.api.AEMScrybeDevice;
import com.aem.api.IAemScrybe;
import com.afollestad.materialdialogs.MaterialDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import visiontech.billcorn.R;
import visiontech.billcorn.animation.CanvasView;
import visiontech.billcorn.database.DatabaseHandler;
import visiontech.billcorn.home.AgentHistoryActivity;
import visiontech.billcorn.model.Transaction;
import visiontech.billcorn.netwrok.AppWebService;
import visiontech.billcorn.util.BU;

public class CustomerBillActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    private String customerName, customerMobile, customerStvNo, customerMonthlyBill, customerYealyBill, customerDue, customerAvailable;
    private TextView txtTapToEdit, txtAdvanceDueText, txtCustomerName, txtStvNumber, txtMonthYear, txtMonthAmount, txtDueAdvace, txtSgst, txtCgst, txtTotalAmount;
    private CheckBox checkAdance, checkDue;
    private Button btnPayPrint, btnPay;
    private int month = 0, year = 0, dayOfMonth = 0;
    private float totalPayableAmount = 0, amountTobePaid = 0, advance = 0, due = 0, yearlyAmount = 0, monthlyAmount = 0;
    private boolean isCheckedDue = false, isCheckedAdvance = false, isPayPrint = false;
    private AEMScrybeDevice aemScrybeDevice = null;
    private AEMPrinter aemPrinter = null;
    private DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_bill);
        txtCustomerName = (TextView) findViewById(R.id.txt_customer_name);
        txtStvNumber = (TextView) findViewById(R.id.txt_stv_number);
        txtMonthYear = (TextView) findViewById(R.id.txt_select_month_year);
        txtMonthAmount = (TextView) findViewById(R.id.txt_month_pay_amount);
        txtTapToEdit = (TextView) findViewById(R.id.txt_tap_to_edit);
        checkAdance = (CheckBox) findViewById(R.id.check_advance);
        checkDue = (CheckBox) findViewById(R.id.check_due);
        txtDueAdvace = (TextView) findViewById(R.id.txt_advance_due_amount);
        txtSgst = (TextView) findViewById(R.id.txt_sgst);
        txtCgst = (TextView) findViewById(R.id.txt_cgst);
        txtTotalAmount = (TextView) findViewById(R.id.txt_total_amount);
        txtAdvanceDueText = (TextView) findViewById(R.id.txt_advance_due_text);
        btnPay = (Button) findViewById(R.id.btn_pay);
        btnPayPrint = (Button) findViewById(R.id.btn_pay_print);

        Intent extra = getIntent();
        if (extra != null) {
            customerName = extra.getStringExtra("customerName");
            customerMobile = extra.getStringExtra("customerMobile");
            customerStvNo = extra.getStringExtra("customerStvNo");
            customerMonthlyBill = extra.getStringExtra("customerMonthlyBill");
            customerYealyBill = extra.getStringExtra("customerYealyBill");
            customerDue = extra.getStringExtra("customerDue");
            customerAvailable = extra.getStringExtra("customerAvailable");
            txtCustomerName.setText(customerName);
            txtStvNumber.setText(customerStvNo);
            txtMonthAmount.setText("" + customerMonthlyBill);
            advance = Float.parseFloat(customerAvailable);
            due = Float.parseFloat(customerDue);
            monthlyAmount = Float.parseFloat(customerMonthlyBill);
            yearlyAmount = Float.parseFloat(customerYealyBill);
            txtSgst.setText(getSgstCgst(monthlyAmount) + "");
            txtCgst.setText(getSgstCgst(monthlyAmount) + "");
            txtTotalAmount.setText((getSgstCgst(monthlyAmount) * 2 + monthlyAmount) + "");
            if (advance > 0) {
                amountTobePaid = monthlyAmount - advance;
                checkAdance.setVisibility(View.VISIBLE);
                checkDue.setVisibility(View.GONE);
                txtDueAdvace.setText("₹ " + advance);
                txtAdvanceDueText.setText("Advance Amount: ");
            } else if (due > 0) {
                amountTobePaid = monthlyAmount + due;
                checkAdance.setVisibility(View.GONE);
                checkDue.setVisibility(View.VISIBLE);
                txtDueAdvace.setText("₹ " + due);
                txtAdvanceDueText.setText("Due Amount: ");
            } else {
                amountTobePaid = monthlyAmount;
                txtDueAdvace.setVisibility(View.GONE);
            }
        }

        txtTapToEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MaterialDialog.Builder(CustomerBillActivity.this)
                        .title("Enter Amount")
                        .negativeText("Cancle")
                        .inputRangeRes(2, 20, R.color.red)
                        .inputType(InputType.TYPE_CLASS_NUMBER)
                        .input(null, null, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                float modifiedAmount = Float.parseFloat(input.toString());
                                totalPayableAmount = modifiedAmount;
                                amountTobePaid = totalPayableAmount;
                                if (modifiedAmount > monthlyAmount) {
                                    advance = advance + modifiedAmount - monthlyAmount;
                                    txtMonthAmount.setText("" + modifiedAmount);
                                    txtSgst.setText(getSgstCgst(modifiedAmount) + "");
                                    txtCgst.setText(getSgstCgst(modifiedAmount) + "");
                                    txtTotalAmount.setText((getSgstCgst(modifiedAmount) * 2 + modifiedAmount) + "");
                                    Toast.makeText(CustomerBillActivity.this, "Advance" + advance, Toast.LENGTH_SHORT).show();
                                } else {
                                    due = due + monthlyAmount - modifiedAmount;
                                    txtMonthAmount.setText("" + modifiedAmount);
                                    txtSgst.setText(getSgstCgst(modifiedAmount) + "");
                                    txtCgst.setText(getSgstCgst(modifiedAmount) + "");
                                    txtTotalAmount.setText((getSgstCgst(modifiedAmount) * 2 + modifiedAmount) + "");
                                    Toast.makeText(CustomerBillActivity.this, "due" + due, Toast.LENGTH_SHORT).show();
                                }


                            }
                        }).show();
            }
        });
        checkAdance.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    isCheckedAdvance = true;
                    float amount = monthlyAmount - advance;
                    txtMonthAmount.setText("" + amount);
                    txtSgst.setText(getSgstCgst(amount) + "");
                    txtCgst.setText(getSgstCgst(amount) + "");
                    txtTotalAmount.setText((getSgstCgst(amount) * 2 + amount) + "");
                } else {
                    isCheckedAdvance = false;
                    txtMonthAmount.setText("" + monthlyAmount);
                    txtSgst.setText(getSgstCgst(monthlyAmount) + "");
                    txtCgst.setText(getSgstCgst(monthlyAmount) + "");
                    txtTotalAmount.setText((getSgstCgst(monthlyAmount) * 2 + monthlyAmount) + "");
                }
            }
        });

        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isInternet()) {
                    isPayPrint = false;
                    payBill();
                }
            }
        });

        btnPayPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isInternet()) {
                    isPayPrint = true;
                    aemScrybeDevice = new AEMScrybeDevice(new IAemScrybe() {
                        @Override
                        public void onDiscoveryComplete(ArrayList<String> arrayList) {
                            Log.e("Device name", "Size " + arrayList.size());
                        }
                    });

                    try {
                        aemScrybeDevice.startDiscover(CustomerBillActivity.this);
                        if (aemScrybeDevice.getPairedPrinters().size() > 0) {
                            aemScrybeDevice.connectToPrinter(aemScrybeDevice.getPairedPrinters().get(0).toString());
                            aemPrinter = aemScrybeDevice.getAemPrinter();
                            if (aemScrybeDevice.BtConnStatus()) {
                                payBill();

                                Toast.makeText(CustomerBillActivity.this, "Printer is connected !!", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(CustomerBillActivity.this, "Unable to connect to the printer !", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(CustomerBillActivity.this, "First Paired the printer !", Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        checkDue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    isCheckedDue = true;
                    float amount = monthlyAmount + due;
                    txtMonthAmount.setText("" + amount);
                    txtSgst.setText(getSgstCgst(amount) + "");
                    txtCgst.setText(getSgstCgst(amount) + "");
                    txtTotalAmount.setText((getSgstCgst(amount) * 2 + amount) + "");
                } else {
                    isCheckedDue = false;
                    txtMonthAmount.setText("" + monthlyAmount);
                    txtSgst.setText(getSgstCgst(monthlyAmount) + "");
                    txtCgst.setText(getSgstCgst(monthlyAmount) + "");
                    txtTotalAmount.setText((getSgstCgst(monthlyAmount) * 2 + monthlyAmount) + "");
                }
            }
        });
        txtMonthYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        CustomerBillActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.showYearPickerFirst(true);
                dpd.show(getSupportFragmentManager(), "Datepickerdialog");
            }
        });
    }

    private void sweetAlert() {
        new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Payment has been successfully made !")
                .setContentText("Payment has been successfully made !").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                finish();
            }
        })
                .show();
    }

    class PayCustomerBillTask extends AsyncTask<String, Void, String> {
        public MaterialDialog dialog;
        private Transaction transaction;
        //  private ArrayList<Transaction> list;

        @Override
        protected void onPreExecute() {
            dialog = new MaterialDialog.Builder(CustomerBillActivity.this)
                    .title("Checking Customer")
                    .content("Please wait..")
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Map<String, String> paramList = new HashMap<String, String>();
            paramList.put("method", params[1]);
            paramList.put("agent_phone", params[2]);
            paramList.put("agent_name", params[3]);
            paramList.put("cust_mobile", params[4]);
            paramList.put("month", params[5]);
            paramList.put("year", params[6]);
            paramList.put("year_amount", params[7]);
            paramList.put("month_amount", params[8]);
            paramList.put("pay_amount", params[9]);
            paramList.put("due_amount", params[10]);
            paramList.put("advance_amount", params[11]);
            paramList.put("yearly_remain_amount", params[12]);
            paramList.put("gst_amount", params[13]);
            return AppWebService.sendByPost(params[0], paramList);
        }

        @Override
        protected void onPostExecute(final String result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
                try {
                    if (result != null) {
                        parseUrlData(result);
                    }
                } catch (Exception e) {
                    Log.e("Download data", "error");

                }
            }
            super.onPostExecute(result);
        }

        private void parseUrlData(String result) {
            try {
                //list = new ArrayList<Transaction>();
                JSONObject jsonObject = new JSONObject(result);
                String returnResult = jsonObject.getString("status");
                if (returnResult.equals("fail")) {
                    new SweetAlertDialog(CustomerBillActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Payment failed! ")
                            .setContentText(jsonObject.getString("message")).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            finish();
                        }
                    })
                            .show();
                    Toast.makeText(CustomerBillActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                } else if (returnResult.equals("success")) {
                    SharedPreferences sharedPref = getSharedPreferences("bill_corn_per", Context.MODE_PRIVATE);
                    String businessName = sharedPref.getString("oprator_business_name", null);
                    String opratorAddress = sharedPref.getString("oprator_address", null);
                    String opratorCity = sharedPref.getString("oprator_city", null);
                    String opratorPhone = sharedPref.getString("oprator_phone", null);
                    if (isPayPrint) {
                        try {
                            String billingDate = dayOfMonth + "/" + month + "/" + year;
                            float localOrDueAvailable = 0;
                            if (due > 0) {
                                localOrDueAvailable = -1 * localOrDueAvailable;
                            } else if (advance > 0) {
                                localOrDueAvailable = advance;
                            } else {
                                localOrDueAvailable = 0;
                            }
                            String printString = "          " + businessName + "           \n" +
                                    "          " + opratorAddress + ", " + opratorCity + "             \n" +
                                    "           " + opratorPhone + "               \n" +
                                    "--------------------------------\n" +
                                    " Invoice Number: 1034               \n" +
                                    " Customer Name :  " + customerName + "  \n" +
                                    " STB No:   " + customerStvNo + "           \n" +
                                    " Billing Date : " + billingDate + "      \n" +
                                    "--------------------------------\n" +
                                    "    Wallet Balance:  " + localOrDueAvailable + "  \n" +
                                    "--------------------------------\n" +
                                    "   Total Amount  - " + totalPayableAmount + " /-\n" +
                                    "   SGST          – " + getSgstCgst(totalPayableAmount) + " /-\n" +
                                    "   CGST          – " + getSgstCgst(totalPayableAmount) + " /-\n" +
                                    "   IGST          – 00.00 /-\n" +
                                    "--------------------------------\n" +
                                    "Amount to be pay     - " + (totalPayableAmount + getSgstCgst(totalPayableAmount * 2)) + " /-\n" +
                                    "Amount paid          - " + (amountTobePaid + getSgstCgst(amountTobePaid) * 2) + " /-\n" +
                                    "--------------------------------\n" +
                                    "     Thank You For Business\n" +
                                    "        Powered By BillCorn\n\n\n\n";
                            aemPrinter.print(printString);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    db = new DatabaseHandler(CustomerBillActivity.this);
                    DateFormat df = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
                    Date dateobj = new Date();
                    String agentPhone = sharedPref.getString("agentPhone", null);
                    Transaction transaction = new Transaction();
                    transaction.setId(jsonObject.getInt("row_id"));
                    transaction.setGstAmount(getSgstCgst(totalPayableAmount)*2);
                    transaction.setPaidYear(year);
                    transaction.setPaidMonth(month);
                    transaction.setPaidDay(df.format(dateobj));
                    transaction.setPaidAmount(totalPayableAmount);
                    transaction.setCustomerMobile(customerMobile);
                    transaction.setAgentPhone(agentPhone);
                    db.insertTransactions(transaction);
                    Toast.makeText(CustomerBillActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    sweetAlert();
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

    }

    private boolean isInternet() {
        if (BU.isConnectingToInternet(CustomerBillActivity.this)) {
            return true;
        } else {
            Toast.makeText(CustomerBillActivity.this, "No internet connection.", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private void payBill() {

        float remainYearlyAmount = 0, gstAmount = 0;
        if (isCheckedDue) {
            totalPayableAmount = monthlyAmount + due;
            due = 0;
        } else if (isCheckedAdvance) {
            totalPayableAmount = monthlyAmount - advance;
            advance = 0;
        } else {
            totalPayableAmount = monthlyAmount;
        }

        if (month != 0 && year != 0) {
            try {
                this.btnPay.setEnabled(false);
                this.btnPayPrint.setEnabled(true);
                this.btnPay.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
                this.btnPayPrint.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
                SharedPreferences sharedPref = getSharedPreferences("bill_corn_per", Context.MODE_PRIVATE);
                String agentName = sharedPref.getString("agentName", null);
                String agentPhone = sharedPref.getString("agentPhone", null);
                remainYearlyAmount = yearlyAmount - totalPayableAmount;
                gstAmount = getSgstCgst(totalPayableAmount) * 2;
                new PayCustomerBillTask().execute(AppWebService.BASE_URL, "payCustomerBill", agentPhone, agentName, customerMobile, month + "", year + "", yearlyAmount + "", monthlyAmount + "", totalPayableAmount + "", due + "", advance + "", remainYearlyAmount + "", gstAmount + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(CustomerBillActivity.this, "Please Select month and year", Toast.LENGTH_SHORT).show();
        }
    }

    private float getSgstCgst(float amount) {
        float tax = (amount * 9) / 100;
        return tax;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        this.month = monthOfYear + 1;
        this.year = year;
        this.dayOfMonth = dayOfMonth;
        txtMonthYear.setText(this.month + "/" + this.year);
    }
}
